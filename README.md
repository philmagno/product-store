# Product Application
This application provides an API to manager products and its image.

## Prerequisites
To run that application your computer needs the following program:

* [Maven](https://maven.apache.org/) - Dependency Management
* [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) - Language of the application
 
## Running
The application has two environments: dev and prod.
By default its run in prod. This env does not load any data in database, and hide some debug log of console.

You can run the application with the command (Production):

```
 mvn spring-boot:run -Drun.arguments="--spring.profiles.active=prod"
```

The second env called dev, load some initial values at database when the application is starting.
To run the app in dev mode just run the following command:

```
 mvn spring-boot:run -Drun.arguments="--spring.profiles.active=dev"
```

## Consuming
The application are document by Swagger.
You can access Swagger UI in order to run all the methods of the application by the following URL:

```
	http://localhost:8080/store/index.html
```
### The list endpoints

| Description | HTTP | Path | Params |
| --- | --- | --- | --- |
| List all products   | [GET] | /api/products |  Optional parameters: withSubproducts={true or false} & withImages={true or false}|
| Retrieve product    | [GET] | /api/products/{id} |   Optional parameters: withSubproducts={true or false} & withImages={true or false}|
| List product images | [GET] | /api/products/{id}/images | 
| Insert product | [POST] | /api/products | Optional parameters: parentId={Long}
| Update product  | [PUT] | /api/products/{id} |
| Delete product | [DELETE] | /api/products/{id} |
| Insert product image | [POST] | /api/products/{id}/images |
| Update product image | [PUT] | /api/products/{id}/images |
| Delete product image | [DELETE] | /api/products/{id}/images |

#### JSON Examples
##### Insert new Product

```json
{
    "name": "Red T-Shirt",
    "description": "Beautiful T-Shirt"
}
```

##### Insert new Product with images and new subproducts
```json
{
	"name": "Yellow product",
	"description": "Item with subproduct",
	"subProducts": [
		{	
			"name": "Light Yellow product",
			"description": "Item with subproduct"
		},
		{
			"name": "Yellow and Brown product",
			"description": "Item with subproduct"
		}
	],
	"images": [
			{"type":"Front"},
			{"type":"Back"}
		]
	}
}
```


##### Update Product

```json
{
    "id": "1",
    "name": "Purple T-Shirt",
    "description": "Nice product !"
}
```

##### Insert new Product image

```json
{
    "name": "Red T-Shirt",
    "description": "Beautiful T-Shirt"
}
```

##### Update images

```json
{
    "id": "1",
    "name": "Purple T-Shirt",
    "description": "Nice product !"
}
```

# Application Architecture
The application follows the microservice principle and use Spring Boot to help create the microservices.
The application uses the following libraries and frameworks: 

* Spring Boot  - A Spring framework that help create self-contained applications.
* H2 Memory Database - That provide a DB running in memory, it is used in Prod and Dev environments.
* Spring Data JPA - A Spring framework used in DAO layer, its makes easy and less verbose create DAOs.
* Jersey (JAX-RS) - Implements the JAX-RS specification.
* Lombok - Makes Java less verbose and more clear with some Annotations, like @Getter @Setter that create getters and setters of the class.

## Packages
The packages of the application was divided by his responsibilities.
This packages are found inside the com.avenuecode.store:

* ProductStoreApplication.java : The main class of Spring Boot that start the application.
* conf : Contains all classes that configure the application. 
* dao : Classes with access to database.
* entity : Model classes of the application (Product and Image). 
* exception : Custom Exceptions that handle unexpected behavior.
* mapper : Custom exception mapper.
* rest : Package with the rest endpoints 
* service : Package that group classes with business rules. 

