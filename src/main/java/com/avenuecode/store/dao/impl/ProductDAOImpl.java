package com.avenuecode.store.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.store.dao.ProductDAO;
import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;

@Repository
@Transactional(readOnly = true)
public class ProductDAOImpl implements ProductDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Product save(Product product) {
		if (product.getId() != null) {
			return entityManager.merge(product);
		}else {
			entityManager.persist(product);
		}

		return product;
	}

	@Override
	@Transactional
	public void delete(Product product) {
		Query query = entityManager.createQuery("DELETE FROM Product where id = :id").setParameter("id", product.getId());
		query.executeUpdate();
	}

	@Override
	public Product findById(Long id) {
		return entityManager.find(Product.class, id);
	}

	@Deprecated
	@Override
	public List<Product> findProductByCriteria(Boolean withSubproducts, Boolean withImages, Long parentId) {
		List<Product> products = createQuery(withSubproducts, withImages, parentId).getResultList();
		
		return products;
	}

	private TypedQuery<Product> createQuery(Boolean withSubproducts, Boolean withImages, Long parentId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Product> cQuery = cb.createQuery(Product.class);
		Root<Product> root = cQuery.from(Product.class);
		
		if (withSubproducts && withImages) {
			cb.construct(Product.class, root.get("id"), root.get("name"), root.get("description"), root.get("parentProduct"), root.get("images"));
		}else if (withSubproducts) {
			cb.construct(Product.class, root.get("id"), root.get("name"), root.get("description"),root.get("parentProduct"));
		}else if (withImages) {
			cb.construct(Product.class, root.get("id"), root.get("name"), root.get("description"), root.get("images"));
		}else {
			cb.construct(Product.class, root.get("id"), root.get("name"), root.get("description"));
		}
		
		if (parentId != null && parentId > 0) {
			Subquery<Long>subquery= cQuery.subquery(Long.class);
			Root<Product> subRoot = subquery.from(Product.class);
			subquery.select(subRoot.get("id"));
			Join<Object, Object> join = subRoot.join("parentProduct", JoinType.INNER);
			
			cQuery.where(cb.in(root.get("id")).value(subquery).not());
			subquery.where(cb.equal(join.get("id"), parentId));
		}
		
		TypedQuery<Product> query = entityManager.createQuery(cQuery);
		return query; 
	}

	@Override
	public List<Product> findProductsByFilter(Long parentId) {
		String hql = "SELECT new Product(p.id, p.name, p.description) from Product p";
		
		if(parentId != null && parentId > 0) {
			hql.concat(" p where p.id = :id ");
		}
		
		TypedQuery<Product> query = entityManager.createQuery(hql.toString(), Product.class);

		if(parentId != null && parentId > 0) {
			query.setParameter("id", parentId);
		}
		
		return query.getResultList();
	}

	@Override
	public List<Product> findSubProducts(Long productId) {
		String hql = "SELECT new Product(sub.id, sub.name, sub.description) from Product p "
				+ " INNER JOIN p.subProducts sub"
				+ " where p.id = :id ";
		Query query = entityManager.createQuery(hql.toString());

		return query.setParameter("id", productId).getResultList();
	}

	@Override
	public List<Image> findProductImages(Long productId) {
		String hql = "SELECT new Image(i.id, i.type) FROM Product p"
				+ " INNER JOIN p.images i "
				+ " WHERE p.id = :productId";
		TypedQuery<Image> query = entityManager.createQuery(hql.toString(), Image.class);
		
		return query.setParameter("productId", productId).getResultList();
	}

}
