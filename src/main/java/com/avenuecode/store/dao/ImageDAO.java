package com.avenuecode.store.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenuecode.store.entity.Image;

public interface ImageDAO extends JpaRepository<Image, Long> {
	
}
