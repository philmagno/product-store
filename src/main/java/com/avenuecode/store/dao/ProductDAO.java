package com.avenuecode.store.dao;

import java.util.List;

import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;

public interface ProductDAO {

	Product save(Product product);

	void delete(Product product);

	List<Product> findProductByCriteria(Boolean withSubproducts, Boolean withImages, Long parentId);

	List<Product> findProductsByFilter(Long parentId);

	Product findById(Long id);

	List<Product> findSubProducts(Long parentId);

	List<Image> findProductImages(Long id);

}
