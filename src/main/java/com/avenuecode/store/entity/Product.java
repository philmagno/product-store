package com.avenuecode.store.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product implements Serializable {
	private static final long serialVersionUID = 5381059624158902631L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Length(min = 5, max = 255, message = "The name of product need to be have the length between {min} and {max} caracteres")
	private String name;

	@NotNull
	@Length(min = 10, max = 500, message = "The description of product need to be have the length between {min} and {max} caracteres")
	private String description;

	@OneToMany(fetch=FetchType.EAGER ,cascade = CascadeType.ALL)
	private List<Product> subProducts;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Image> images;

	public Product(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Product(Long id, String name, String description, List<Product> subProducts) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.subProducts = subProducts;
	}
	
	public Product(Long id, String name, String description, Collection<Product> subProducts) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public void addSubProduct(Product product) {
		if (subProducts == null) {
			subProducts = new ArrayList<>();
		}

		subProducts.add(product);
	}

}
