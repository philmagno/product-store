package com.avenuecode.store.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.avenuecode.store.exception.ProductNotFoundException;

import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Provider
public class ProductNotFoundMapper implements ExceptionMapper<ProductNotFoundException> {

	@Override
	public Response toResponse(ProductNotFoundException exception) {
		return Response.status(NOT_FOUND)
				.entity("Product not found, please verify the id of Product")
				.type(TEXT_PLAIN)
				.build();
	}
}
