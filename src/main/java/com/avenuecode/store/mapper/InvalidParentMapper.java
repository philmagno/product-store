package com.avenuecode.store.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.avenuecode.store.exception.InvalidParentException;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@Provider
public class InvalidParentMapper implements ExceptionMapper<InvalidParentException> {

	@Override
	public Response toResponse(InvalidParentException exception) {
		return Response.status(BAD_REQUEST)
				.entity("The product create a cycle with him self")
				.type(TEXT_PLAIN)
				.build();
	}

}
