package com.avenuecode.store.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@Provider
public class UnhandledExceptionMapper implements ExceptionMapper<Exception> {

	private static final Logger log = LoggerFactory.getLogger(UnhandledExceptionMapper.class);

	@Override
	public Response toResponse(Exception exception) {
		log.error("UnhandledException", exception);
		return Response.status(INTERNAL_SERVER_ERROR)
				.type(TEXT_PLAIN)
				.entity(exception.getMessage())
				.build();
	}

}
