package com.avenuecode.store.conf;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.avenuecode.store.rest.ProductController;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@Configuration
public class ApplicationResourceConfig extends ResourceConfig {

	private static final String WS_ROOT = "/api/*";

	@Bean
	public ServletRegistrationBean jerseyServlet() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new ServletContainer(), WS_ROOT);
		registration.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, JerseyServletConfig.class.getName());
		return registration;
	}

	public static class JerseyServletConfig extends ResourceConfig {
		public JerseyServletConfig() {
			packages("com.avenuecode.store.rest");
			register(ProductController.class);
			register(SwaggerSerializers.class);
			configureSwagger();
		}

		private void configureSwagger() {
			register(ApiListingResource.class);
			BeanConfig beanConfig = new BeanConfig();
			beanConfig.setVersion("0.0.1");
			beanConfig.setSchemes(new String[] { "http" });
			beanConfig.setHost("localhost:8080");
			beanConfig.setBasePath("/store/api");
			beanConfig.setDescription("Sample");
			beanConfig.setContact("VIIGit");
			beanConfig.setResourcePackage("com.avenuecode.store.rest");
			beanConfig.setPrettyPrint(true);
			beanConfig.setScan(true);
		}
	}

}
