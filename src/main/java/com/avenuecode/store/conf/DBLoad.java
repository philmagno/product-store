package com.avenuecode.store.conf;

import java.util.Arrays;
import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.avenuecode.store.dao.ImageDAO;
import com.avenuecode.store.dao.ProductDAO;
import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;

import lombok.extern.java.Log;

@Component
@Log
@Profile("dev")
public class DBLoad implements ApplicationRunner {

	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private ImageDAO imageDAO;

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		createProductsInDevDB();
		createImagesInDevDB();
	}

	private void createProductsInDevDB() {
		log.log(Level.INFO, "Loading all products information in database");
		
		Product product01 = Product.builder()
				.name("Product 01")
				.description("Amazing Product")
				.images(Arrays.asList(Image.builder().type("teste").build()))
				.build();

		Product product02 = Product.builder()
				.name("Product 02")
				.description("Awesome Product")
				.build();
		
		Product product03 = Product.builder()
				.name("Product 02")
				.description("Not Good product")
				.build();
		
		productDAO.save(product01);
		productDAO.save(product02);
		productDAO.save(product03);
	}
	
	private void createImagesInDevDB() {
		Image image01 = Image.builder()
				.type("E-book")
				.build();
		
		Image image02 = Image.builder()
				.type("Online Course")
				.build();
		
		imageDAO.save(image01);
		imageDAO.save(image02);
	}

}
