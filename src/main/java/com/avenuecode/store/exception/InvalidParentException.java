package com.avenuecode.store.exception;

public class InvalidParentException extends RuntimeException {

	private static final long serialVersionUID = -4322112605793761002L;

	public InvalidParentException() {
		super();
	}

	public InvalidParentException(String s) {
		super(s);
	}

	public InvalidParentException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public InvalidParentException(Throwable throwable) {
		super(throwable);
	}

}
