package com.avenuecode.store.exception;

public class ImageNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7552239701657112188L;

	public ImageNotFoundException() {
		super();
	}

	public ImageNotFoundException(String s) {
		super(s);
	}

	public ImageNotFoundException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public ImageNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
