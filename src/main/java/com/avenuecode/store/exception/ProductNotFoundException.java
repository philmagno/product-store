package com.avenuecode.store.exception;

public class ProductNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 2942153963687346408L;

	public ProductNotFoundException() {
		super();
	}

	public ProductNotFoundException(String s) {
		super(s);
	}

	public ProductNotFoundException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public ProductNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
