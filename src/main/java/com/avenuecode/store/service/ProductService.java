package com.avenuecode.store.service;

import java.util.List;

import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;
import com.avenuecode.store.exception.InvalidParentException;
import com.avenuecode.store.exception.ProductNotFoundException;

public interface ProductService {

	List<Product> retrieveProducts(Boolean withSubproducts, Boolean withImages, Long parentId);

	Product saveProduct(Product product, Long id) throws InvalidParentException;

	void updateProduct(Product product) throws ProductNotFoundException;

	void delete(Long id) throws ProductNotFoundException;

	List<Product> retrieveProductWithSubProducts(Long parentProductId);
	
	List<Image> retrieveProductImages(Long productId);

}
