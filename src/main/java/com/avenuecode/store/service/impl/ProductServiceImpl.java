package com.avenuecode.store.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.store.dao.ProductDAO;
import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;
import com.avenuecode.store.exception.InvalidParentException;
import com.avenuecode.store.exception.ProductNotFoundException;
import com.avenuecode.store.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;
	
	@Override
	public List<Product> retrieveProducts(Boolean withSubproducts, Boolean withImages, Long parentId) {
		List<Product> products = productDAO.findProductsByFilter(parentId);
		loadRelationship(products, withSubproducts, withImages);
		
		return products;
	}

	private void loadRelationship(List<Product> products, Boolean withSubproducts, Boolean withImages) {
		if (withSubproducts) {
			products.forEach(product -> {
				product.setSubProducts(productDAO.findSubProducts(product.getId()));
			});
		}

		if (withImages) {
			products.forEach(product -> {
				product.setImages(productDAO.findProductImages(product.getId()));
			});
		}
	}

	@Override
	public Product saveProduct(Product product, Long parentProductId) throws InvalidParentException {
		Product parentProduct = null;

		if (parentProductId != null && parentProductId > 0) {
			parentProduct = productDAO.findById(parentProductId);

			if (parentProduct == null) {
				throw new InvalidParentException("Parent product not found!");
			}

			verifyIfCreateACycle(product, parentProduct);
		}

		if (parentProduct != null) {
			Product persistedProduct = productDAO.save(product);
			parentProduct.addSubProduct(persistedProduct);
			productDAO.save(parentProduct);
			return persistedProduct;
		}

		return productDAO.save(product);
	}

	private void verifyIfCreateACycle(Product product, Product parentProduct) {
		if(product.getSubProducts() != null) {
			boolean createACycle = false;
			product.getSubProducts().stream()
			.filter(p -> p.getId() != null)
			.filter(p -> p.getId() == parentProduct.getId())
			.findFirst().ifPresent(e ->  {
				throw new InvalidParentException("This producte create a cycle");
			});
		}
	}

	@Override
	public void updateProduct(Product product) throws ProductNotFoundException {
		Product retrivedProduct = productDAO.findById(product.getId());
		
		if (retrivedProduct == null) {
			log.error("Product not found : ID " + product.getId());
			throw new ProductNotFoundException();
		}

		retrivedProduct.setDescription(product.getDescription());
		retrivedProduct.setName(product.getName());
		
		productDAO.save(retrivedProduct);
	}

	@Override
	public void delete(Long id) throws ProductNotFoundException {
		Product product = productDAO.findById(id);

		if (product == null) {
			log.error("Product not found : ID " + id);
			throw new ProductNotFoundException();
		}

		productDAO.delete(product);
	}

	@Override
	public List<Product> retrieveProductWithSubProducts(Long parentId) {
		return productDAO.findProductByCriteria(false, false, parentId);
	}

	@Override
	public List<Image> retrieveProductImages(Long productId) {
		return productDAO.findProductImages(productId);
	}

}
