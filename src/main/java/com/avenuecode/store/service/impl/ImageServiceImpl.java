package com.avenuecode.store.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.store.dao.ImageDAO;
import com.avenuecode.store.entity.Image;
import com.avenuecode.store.exception.ImageNotFoundException;
import com.avenuecode.store.service.ImageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageDAO imageDAO;
	
	@Override
	public List<Image> retrieveImages(Long id){
		return imageDAO.findAll();
	}

	@Override
	public Image saveImage(Image image) {
		Image persistedImage = imageDAO.saveAndFlush(image);
		return persistedImage;
	}
	
	@Override
	public void updateImage(Image image) throws ImageNotFoundException {
		Image retrivedImage = imageDAO.findOne(image.getId());
		
		if(retrivedImage == null) {
			log.error("Image not found: ID" + image.getId());
			throw new ImageNotFoundException();
		}
		
		retrivedImage.setType(image.getType());
		
		imageDAO.saveAndFlush(retrivedImage);
	}

	@Override
	public void deleteImage(Long imageId) throws ImageNotFoundException {
		Image image = imageDAO.findOne(imageId);
		
		if(image == null) {
			throw new ImageNotFoundException("Image not found: ID" + imageId);
		}
		
		imageDAO.delete(imageId);
	}
}
