package com.avenuecode.store.service;

import java.util.List;

import com.avenuecode.store.entity.Image;
import com.avenuecode.store.exception.ImageNotFoundException;

public interface ImageService {

	Image saveImage(Image image);

	void updateImage(Image image) throws ImageNotFoundException;

	List<Image> retrieveImages(Long id);

	void deleteImage(Long imageId) throws ImageNotFoundException;

}
