package com.avenuecode.store.rest;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.avenuecode.store.entity.Image;
import com.avenuecode.store.entity.Product;
import com.avenuecode.store.service.ImageService;
import com.avenuecode.store.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api()
@Component
@Path("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private ImageService imageService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RequestMapping(method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "List all products", notes = "This method list all products")
	public List<Product> retriveAllProduct(
			@DefaultValue("false") @QueryParam("withSubproducts") Boolean withSubproducts,
			@DefaultValue("false") @QueryParam("withImages") Boolean withImages) {
		List<Product> products = productService.retrieveProducts(withSubproducts, withImages, null);
		return products;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> retrieveSubProducts(@PathParam("id") Long productId,
			@DefaultValue("false") @QueryParam("withSubproducts") Boolean withSubproducts,
			@DefaultValue("false") @QueryParam("withImages") Boolean withImages) {
		List<Product> products = productService.retrieveProducts(withSubproducts, withImages, productId);
		return products;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product createNewProduct(Product product, @QueryParam("parentProduct") Long id) {
		Product savedProduct = productService.saveProduct(product, id);
		return savedProduct;
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(@NotNull @PathParam("id") Long id, Product product) {
		product.setId(id);
		productService.updateProduct(product);
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteProduct(@NotNull @PathParam("id") Long id) {
		productService.delete(id);
		return Response.ok().build();
	}

	@GET
	@Path("/{id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	public Response retrieveAllImages(@NotNull @PathParam("id") Long id) {
		List<Image> images = productService.retrieveProductImages(id);
		return Response.ok().entity(images).build();
	}

	@POST
	@Path("/{id}/images")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateImage(@NotNull @PathParam("id") Long id, Image image) {
		imageService.updateImage(image);
		return Response.noContent().build();
	}

	@PUT
	@Path("/{id}/images")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveImage(@NotNull @PathParam("id") Long id, Image image) {
		Image savedImage = imageService.saveImage(image);
		return Response.ok().entity(savedImage).build();
	}

	@DELETE
	@Path("/{productId}/images/{imageId}")
	public Response deleteImage(@NotNull @PathParam("productId") Long productId, @NotNull @PathParam("imageId") Long imageId) {
		imageService.deleteImage(imageId);
		return Response.noContent().build();
	}

}
