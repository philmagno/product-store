package com.avenuecode.store.unit;

import static com.avenuecode.store.data.TestProducts.defaultProduct;
import static com.avenuecode.store.data.TestProducts.defaultSavedProduct;
import static com.avenuecode.store.data.TestProducts.productWithId;
import static com.avenuecode.store.data.TestProducts.subProducts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.avenuecode.store.dao.ProductDAO;
import com.avenuecode.store.entity.Product;
import com.avenuecode.store.exception.InvalidParentException;
import com.avenuecode.store.exception.ProductNotFoundException;
import com.avenuecode.store.service.ProductService;
import com.avenuecode.store.service.impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceUnitTest {
	
	@Mock
	private ProductDAO productDAO;
	
	@InjectMocks
	private ProductService productService = new ProductServiceImpl();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shoulSaveANewProduct() throws InvalidParentException {
		Product product = defaultProduct();
		
		when(productDAO.save(product)).thenReturn(defaultSavedProduct());

		Product savedProduct = productService.saveProduct(product, null);
		
		assertNotNull(savedProduct);
		assertNotNull(savedProduct.getId());
		assertEquals(product.getName(), savedProduct.getName());
		assertEquals(product.getDescription(), savedProduct.getDescription());
	}
	
	@Test(expected = InvalidParentException.class)
	public void shouldSaveProductWithInvalidParent() throws InvalidParentException {
		Product product = defaultProduct();
		
		when(productDAO.findById(1L)).thenReturn(null);
		when(productDAO.save(product)).thenReturn(defaultSavedProduct());

		productService.saveProduct(product, 1L);
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void shouldUpdateAInvalidProduct() throws ProductNotFoundException {
		Product product = defaultProduct();
		
		when(productDAO.findById(100L)).thenReturn(null);
		
		productService.updateProduct(product);
	}

	
}
