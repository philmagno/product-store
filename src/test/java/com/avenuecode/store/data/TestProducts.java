package com.avenuecode.store.data;

import java.util.Arrays;
import java.util.List;

import com.avenuecode.store.entity.Product;

public class TestProducts {
	
	public static Product defaultProduct() {
		return Product.builder()
				.name("Product 01")
				.description("unit product")
				.build();
	}
	
	public static Product productWithId(Long id) {
		return Product.builder()
				.id(id)
				.name("Product 01")
				.description("unit product")
				.build();
	}
	
	public static Product defaultSavedProduct() {
		return productWithId(1L);
	}
	
	public static List<Product> subProducts() {
		Product product01 = productWithId(1L);
		Product product02 = productWithId(2L);
		Product product03 = productWithId(3L);
		
		return Arrays.asList(product01, product02, product03);
	}
}
